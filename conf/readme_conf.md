# `/conf` file description

## Overview

In this `/conf` directory, you can find  the different `.config` files that dictate the pipeline's behavior. These configuration files are instrumental in setting up the environment and resources required by the various tools and processes within the pipeline.

## Directory structure 

This section shows the  structure of the `/conf` directory, providing an overview of the Bash scripts used for the different tools of our pipeline.

```
/aconf/
├── base.config
├── custom.config
├── readme_conf.md
├── reports.config
└── ressources.config
```

Each file has a unique function:

- `base.config` : Establishes the default settings for users new to the pipeline.
- `custom.config` : Allows you to specify general input paths and parameters tailored to individual analyses.
- `resources.config` : Specifies computer resources such as RAM, CPU, and execution time allocated to each tool.
- `reports.config` : Configures parameters to generate reports on the workflow, aiding documentation and reproducibility.

*Users are not required to modify these configurations for standard pipeline execution.*

## Licence

This work is licensed under [GNU General Public License v3.0](https://www.gnu.org/licenses/gpl-3.0.en.html#license-text). This license permits others to modify, adjust, and build upon this work for any purpose, provided they credit the author and distribute their derivative creations under the same terms of the GNU General Public License version 3 or later.

## Citation and acknowledgement

### Acknowledgement

This project is part of an introductory course on F.A.I.R. principles at the [University of Rennes](https://www.univ-rennes.fr/). Directed by:

- [Anthony Bretaudeau](https://orcid.org/0000-0003-0914-2470) - INRAE, Rennes
- [Alexandre Cormier](https://orcid.org/0000-0002-7775-8413) - Ifremer, IRSI-SeBiMER, Plouzané, France
- [Erwan Corre](https://orcid.org/0000-0001-6354-2278) - CNRS, Sorbonne Université, ABiMS, Station Biologique, Roscoff, France
- [Patrick Durand](https://orcid.org/0000-0002-5597-4457) - Ifremer, IRSI-SeBiMER, Plouzané, France
- [Gildas  Le Corguillé](https://orcid.org/0000-0003-1742-9711) - Sorbonne Université, CNRS, ABiMS, Station Biologique, Roscoff, France
- [Stéphanie Robin](https://orcid.org/0000-0001-7379-9173) - INRAE, Rennes

Special thanks to the authors of the software packages, pipeline, and public datasets used in this project.

### Citation

For citing this project, refer to the following:

- Project subject on [Ifremer's teaching material](https://practical-case-bioinfo-teaching-fair-gaa-92d332f8346e321adf6fde.gitlab-pages.ifremer.fr/#working-data-sets).
- Associated [training material](https://bioinfo.gitlab-pages.ifremer.fr/teaching/fair-gaa/practical-case/).
- [F.A.I.R principles course](https://fair-gaa-bioinfo-teaching-fair-gaa-4217911ae679179702c067b9f84e.gitlab-pages.ifremer.fr/).

Data sourced from WELLCOME SANGER INSTITUTE, GenBank assembly GCA_914767535.2, processed by ABiMS, Genouest, SeBiMER in January 2024.

This project is registered on [ZENODO](https://zenodo.org/records/10607037), DOI : 10.5281/zenodo.10607037.

## Date

2024-02-02

## Authors

- Maxime Fleury, Master's student in Bioinformatics, University of Rennes - maxime.fleury.1@etudiant.univ-rennes.fr
- Adel Fodil-Cherif, Master's student in Bioinformatics, University of Rennes - adel.fodil-cherif@etudiant.univ-rennes.fr
