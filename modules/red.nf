process red {

    label 'midmem'

    tag "red"

    publishDir "${params.resultdir}/03_red",		mode: 'copy', pattern: 'masked/*.msk'
    publishDir "${params.resultdir}/logs/red",		mode: 'copy', pattern: 'red*.log'
    publishDir "${params.outdir}/00_pipeline_info/cmd",	mode: 'copy', pattern: 'red*.cmd'

    input:
	path(assembly_fa)

    output:
        path("masked/*.msk"), emit: red_ch
        path("red*.log")
        path("red*.cmd")

    script:
    """
    red.sh $assembly_fa ${task.cpus} red.cmd  >& red.log 2>&1
    """ 
}
