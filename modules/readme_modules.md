# `/modules` file description

## Overview

In this `/modules` directory, you can find  the different `.nf` files in Nextflow language. These files define the different processes executed during the pipeline's workflow. 

## Directory structure 

This section shows the  structure of the `/modules` directory, providing an overview of the Nextflow scripts used for the different tools of our pipeline.

```
/modules/
├── busco.nf
├── fastqc.nf
├── hifiasm.nf
├── hisat2.nf
├── multiqc.nf
├── nanoplot.nf
├── readme_modules.md
├── red.nf
└── samtools.nf
```

## Licence

This work is licensed under [GNU General Public License v3.0](https://www.gnu.org/licenses/gpl-3.0.en.html#license-text). This license permits others to modify, adjust, and build upon this work for any purpose, provided they credit the author and distribute their derivative creations under the same terms of the GNU General Public License version 3 or later.

## Citation and acknowledgement

### Acknowledgement

This project is part of an introductory course on F.A.I.R. principles at the [University of Rennes](https://www.univ-rennes.fr/). Directed by:

- [Anthony Bretaudeau](https://orcid.org/0000-0003-0914-2470) - INRAE, Rennes
- [Alexandre Cormier](https://orcid.org/0000-0002-7775-8413) - Ifremer, IRSI-SeBiMER, Plouzané, France
- [Erwan Corre](https://orcid.org/0000-0001-6354-2278) - CNRS, Sorbonne Université, ABiMS, Station Biologique, Roscoff, France
- [Patrick Durand](https://orcid.org/0000-0002-5597-4457) - Ifremer, IRSI-SeBiMER, Plouzané, France
- [Gildas  Le Corguillé](https://orcid.org/0000-0003-1742-9711) - Sorbonne Université, CNRS, ABiMS, Station Biologique, Roscoff, France
- [Stéphanie Robin](https://orcid.org/0000-0001-7379-9173) - INRAE, Rennes

Special thanks to the authors of the software packages, pipeline, and public datasets used in this project.

### Citation

For citing this project, refer to the following:

- Project subject on [Ifremer's teaching material](https://practical-case-bioinfo-teaching-fair-gaa-92d332f8346e321adf6fde.gitlab-pages.ifremer.fr/#working-data-sets).
- Associated [training material](https://bioinfo.gitlab-pages.ifremer.fr/teaching/fair-gaa/practical-case/).
- [F.A.I.R principles course](https://fair-gaa-bioinfo-teaching-fair-gaa-4217911ae679179702c067b9f84e.gitlab-pages.ifremer.fr/).

Data sourced from WELLCOME SANGER INSTITUTE, GenBank assembly GCA_914767535.2, processed by ABiMS, Genouest, SeBiMER in January 2024.

This project is registered on [ZENODO](https://zenodo.org/records/10607037), DOI : 10.5281/zenodo.10607037.

## Date

2024-02-02

## Authors

- Maxime Fleury, Master's student in Bioinformatics, University of Rennes - maxime.fleury.1@etudiant.univ-rennes.fr
- Adel Fodil-Cherif, Master's student in Bioinformatics, University of Rennes - adel.fodil-cherif@etudiant.univ-rennes.fr
