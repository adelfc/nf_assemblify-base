process samtools {

    label 'highmem'

    tag "samtools"

    publishDir "${params.resultdir}/05_samtools",		mode: 'copy', pattern: '*.bai'
    publishDir "${params.resultdir}/logs/samtools",	mode: 'copy', pattern: 'samtools*.log'
    publishDir "${params.outdir}/00_pipeline_info/cmd",	mode: 'copy', pattern: 'samtools*.cmd'

    input:
	path(bam_ch)

    output:
	path("*.bai")
	path("samtools*.log")
	path("samtool*.cmd")
        

    script:
    """
    samtools.sh ${bam_ch} ${task.cpus}  samtools.cmd  >& samtools.log 2>&1
    """ 
}
