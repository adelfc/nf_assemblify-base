process hisat2 {

    label 'highmem'

    tag "hisat2"

    publishDir "${params.resultdir}/04_hisat2",		mode: 'copy', pattern: '{*.bam,*.ht2}'
    publishDir "${params.resultdir}/logs/hisat2",	mode: 'copy', pattern: 'hisat2*.log'
    publishDir "${params.outdir}/00_pipeline_info/cmd",	mode: 'copy', pattern: 'hisat2*.cmd'

    input:
	path(illumina_r1_ch)
	path(illumina_r2_ch)
	val(intron)
	path(red_ch)

    output:
	path("*.ht2")
	path("*.bam"), emit: bam_ch 
	path("hisat2*.log")
        path("hisat2*.cmd")

        

    script:
    """
    hisat2.sh ${task.cpus} ${red_ch} ${intron} ${illumina_r1_ch} ${illumina_r2_ch} hisat2.cmd  >& hisat2.log 2>&1
    """ 
}


