![Licence: GPL v3](https://img.shields.io/badge/Licence-GPLv3-brightgreen?style=flat)
![DOI](https://img.shields.io/badge/DOI-10.5281/zenodo.10607037-blue?style=flat)
![Last Update](https://img.shields.io/badge/Last_Update-February_2024-orange?style=flat)
![Language: Bash & Nextflow](https://img.shields.io/badge/Language-Bash_&_Nextflow-critical?style=flat)
![Nextflow Version](https://img.shields.io/badge/Nextflow-%3E%3D23.04.0-9a0eea?style=flat)


# **Assemblify-base pipeline : FAIR principles applied to genome assembly and annotation**

[TOC]

## Introduction

### Overview and goals

The primary aim of this project is to develop a Nextflow-based pipeline focused on genome assembly and annotation, specifically targeting the Dunaliella primolecta species. This initiative is guided by the FAIR principles, ensuring that our approach and methods are Findable, Accessible, Interoperable, and Reusable.

Dunaliella primolecta, a significant species in the realm of genomic studies, is our study model. Our project uses Nextflow, a powerful workflow tool, to efficiently manage the complex processes involved in genome assembly and annotation.

### FAIR principles

This project adheres to the FAIR principles, ensuring that our data and pipeline are Findable, Accessible, Interoperable, and Reusable. For a detailed understanding of these principles, please refer to the official [French Government website on FAIR principles](https://www.ccsd.cnrs.fr/principes-fair/) and the concise summary available on [Open Science](https://www.ouvrirlascience.fr/fair-principles/).

<img src="img/fair_circle.png" alt="" width="200"/>

### Data presentation

The data used in this project encompasses various genomic sequences and information specific to Dunaliella primolecta, as detailed in the [European Nucleotide Archive (ENA)](https://www.ebi.ac.uk/ena/browser/home). The complete and subset data include:

- **Full Data [(Table 3)](https://practical-case-bioinfo-teaching-fair-gaa-92d332f8346e321adf6fde.gitlab-pages.ifremer.fr/#full)**:
  - Study Accession: PRJEB46283
  - Sample Accessions: SAMEA7524530, SAMEA8100059
  - Run Accessions: ERR6688544 to ERR6688549, ERR6939244
  - Library Construction Protocols: RNA PolyA, Chromium Genome, Hi-C - Arima v2, PacBio - HiFi

- **Subset Data [(Table 4)](https://practical-case-bioinfo-teaching-fair-gaa-92d332f8346e321adf6fde.gitlab-pages.ifremer.fr/#subset)**:
  - Study Accession: PRJEB46283
  - Sample Accession: SAMEA7524530, SAMEA8100059
  - Run Accessions: ERR6688549, ERR6939244
  - Library Construction Protocols: RNA PolyA, PacBio - HiFi

The data files can be downloaded from [ENA's website](https://www.ebi.ac.uk/ena/browser/home). The total size of the downloaded files is approximately 93 GB. To reduce calculation time, we only use a subset of the Illumina and PacBio sequencing data, resulting in a dataset of about 4 GB.

For more details about the data, you can explore these websites:

- [Genome Data on NCBI](https://www.ncbi.nlm.nih.gov/datasets/genome/GCA_914767535.2/)
- [Darwin Tree of Life - Dunaliella primolecta](https://tolqc.cog.sanger.ac.uk/darwin/algae/Dunaliella_primolecta/)
- [Darwin Tree of Life Portal](https://portal.darwintreeoflife.org/data/root/details/Dunaliella%20primolecta)

## Pipeline overview

![Pipeline overview](img/pipeline_overview.png)

### Compute requirement

This project was created, developed, and launched on the [IFB cluster](https://login.cluster.france-bioinformatique.fr/dex/auth/ldap/login?back=&state=okszu3py62wfulvummfrqflyv) in the Jupyter-noteBook section. The environment parameters are as follows:

- Kernel: Bash
- Partition: `fast`
- Number of CPUs: 2
- Amount of memory: 8G
- GPUs: No GPU (0 GPUs)

The genome assembly and annotation pipeline is automated using the [Nextflow](https://www.nextflow.io/docs/latest/index.html) language for versions greater than or equal to version `23.04.0`. To ensure that the pipeline and its programs run correctly with the correct versioning, we use [Singularity](https://sylabs.io/singularity/) to build images. Images are generated using [Quay](https://quay.io/search) and [BioContainers](https://biocontainers.pro/registry).

### Container links

The following containers are used in the pipeline:

- **fastqc:** `quay.io/biocontainers/fastqc:0.11.9--hdfd78af_1`
- **multiqc:** `quay.io/biocontainers/multiqc:1.14--pyhdfd78af_0`
- **nanoplot:** `quay.io/biocontainers/nanoplot:1.32.1--py_0`
- **hifiasm:** `quay.io/biocontainers/hifiasm:0.18.9--h5b5514e_0`
- **busco:** `quay.io/biocontainers/busco:5.5.0--pyhdfd78af_0`
- **Red:** `quay.io/biocontainers/red:2018.09.10--h4ac6f70_2`
- **Hisat2:** `quay.io/biocontainers/hisat2:2.2.1--hdbdd923_6`
- **samtools:** `quay.io/biocontainers/samtools:1.19.2--h50ea8bc_0`
- **braker3:** `teambraker/braker3:latest`

### Installing the project on the machine

*Project installation is explained in the context of using the [IFB](https://login.cluster.france-bioinformatique.fr/dex/auth/ldap/login?back=&state=okszu3py62wfulvummfrqflyv) cluster. If you're using another computing environment, then skip to step 3.*

#### Step 1: Forking the project

Navigate to [nf_assemblify-base on GitLab](https://gitlab.com/ifb-elixirfr/training/workflows/nf_assemblify-base). Log in with your GitLab account and fork the project to your namespace by clicking on **Fork**.


#### Step 2: Setting up the environment

Initialize your project workspace on the IFB cluster:

```
cd /shared/projects/2401_m2_bim
cd app-2401-XXX # Replace XXX with your specific identifier
mkdir assemblify-tmp # This directory will serve as the pipeline's working directory
```

#### Step 3: Cloning the repository

Clone the forked repository to your local environment. For HTTPS:
```
git clone https://gitlab.com/<your_username>/nf_assemblify-base.git
```
Or for SSH:
```
git clone git@gitlab.com:<your_username>/nf_assemblify-base.git
```

#### Step 4: Project directory

Change to the project directory:

```
cd nf_assemblify-base/
```

#### Step 5: Main stable branch

Ensure you are on the master branch, the main stable branch of the project:

```
git checkout master
git branch
```

## Project structure

This section shows the directory structure and key components of the Assemblify-base pipeline project, providing an overview of the files and folders essential for its operation:

```
nf_assembly-base/
├── bin/ # Contains bash scripts for individual tools used in the pipeline
│ ├── fastqc.sh
│ ├── hifiasm.sh
│ ├── multiqc.sh
│ ├── nanoplot.sh
│ ├── busco.sh
│ ├── red.sh
│ ├── hisat.sh
│ ├── samtools.sh
│ └── readme_bin.md # Instructions or notes for the bin directory
├── conf/ # Configuration files for running the pipeline
│ ├── base.config
│ ├── custom.config
│ ├── reports.config 
│ ├── resources.config 
│ └── readme_conf.md 
├── dev/ # Under development : Braker3 scripts
│ ├── braker.nf
│ └── braker.sh
├── modules/ # Nextflow modules, each defining a process in the pipeline
│ ├── fastqc.nf
│ ├── hifiasm.nf 
│ ├── multiqc.nf 
│ ├── nanoplot.nf 
│ ├── busco.nf 
│ ├── red.nf
│ ├── hisat.nf 
│ ├── samtools.sh 
│ └── readme_modules.md 
├── scripts/ # Scripts for submitting jobs to a cluster or managing tasks
│ ├── 01a-fastqc.pbs
│ ├── 01b-multiqc.pbs
│ ├── 01c-nanoplot.pbs
│ ├── 02a-hifiasm.pbs
│ ├── 02b-busco.pbs
│ ├── 02c-gfastats.pbs
│ ├── 03-redmask.pbs
│ ├── 04-hisat2.pbs
│ └── readme_scripts.md
├── README.md # The main documentation file for the project
├── clean.sh # Script to clean up workspace before/after runs
├── ifb-core.config # Configuration for running on the IFB core cluster
├── main.nf # Main Nextflow script defining the pipeline workflow
├── nextflow.config # Main configuration file for Nextflow, setting default parameters
└── run_assemblify.slurm # SLURM script for executing the pipeline on a SLURM-based cluster
```

## Presentation of the pipeline's project

This section outlines the stages of our genome assembly and annotation pipeline. Note that we've selectively executed key stages based on a foundational pipeline available on [GitLab](https://gitlab.com/ifb-elixirfr/training/workflows/nf_assemblify-base). Here is a breakdown of the steps, including the tools used, with information on the input and output files and the corresponding scripts located in the `bin/` directory for Bash scripts.

*The `README.md` files within each directory offer additional details and guidelines for using these scripts effectively.*

### Stage 1 - Quality control

#### Nanoplot

- **Tool:** [Nanoplot](https://biocontainer-doc.readthedocs.io/en/latest/source/nanoplot/nanoplot.html) (version 1.32.1), a plotting tool for long-read sequencing data.
- **Input:** PacBio sequencing files (`.fastq.gz`).
- **Output:** Web report (`.html`), plots (`.png`), and statistics (`.txt`).
- **Script:** [`./bin/nanoplot.sh`](https://gitlab.com/adelfc/nf_assemblify-base/-/blob/master/bin/nanoplot.sh)

#### Fastqc

- **Tool:** [FastQC](https://www.bioinformatics.babraham.ac.uk/projects/fastqc/) (version 0.11.9), for quality control of high throughput sequence data.
- **Input:** Illumina sequencing files (`.fastq.gz`).
- **Output:** Web report (`.html`), archive (`.zip`).
- **Script:** [`./bin/fastqc.sh`](https://gitlab.com/adelfc/nf_assemblify-base/-/blob/master/bin/fastqc.sh)

#### Multiqc

- **Tool:** [MultiQC](https://multiqc.info) (version 1.14), aggregates results from bioinformatics analyses into a single report.
- **Input:** Archive from FastQC (`.zip`)
- **Output:** Report quality control (`.html`).
- **Script:** [`./bin/multiqc.sh`](https://gitlab.com/adelfc/nf_assemblify-base/-/blob/master/bin/multiqc.sh)

### Stage 2 - Contiging

#### Hifiasm

- **Tool:** [Hifiasm](https://hifiasm.readthedocs.io/en/latest/) (version 0.18.9), for haplotype-resolved de novo assembly.
- **Input:** PacBio sequencing files (`.fastq.gz`).
- **Output:** Assembly sequences (`.fasta`), assembly graph (`.gfa`).
- **Script:** [`./bin/hifiasm.sh`](https://gitlab.com/adelfc/nf_assemblify-base/-/blob/master/bin/hifiasm.sh)

#### Busco

- **Tool:** [BUSCO](https://busco.ezlab.org) (version 5.5.0), assesses genome assembly completeness.
- **Input:** Assembly sequences from Hifiasm (`.fasta`).
- **Output:** Statistics (`.tsv`).
- **Script:** [`./bin/busco.sh`](https://gitlab.com/adelfc/nf_assemblify-base/-/blob/master/bin/busco.sh)

### Stage 3 - Repeat Masking

#### RED

- **Tool:** REDmask (version 2018.09.10), identifies and masks repeats for structural annotation.
- **Input:** Assembly sequences from Hifiasm (`.fasta`).
- **Output:** Softmasked assembly sequences (`.msk`).
- **Script:** [`./bin/red.sh`](https://gitlab.com/adelfc/nf_assemblify-base/-/blob/master/bin/red.sh)

### Stage 4 - Evidences

#### Hisat2

- **Tool:** [Hisat](http://daehwankimlab.github.io/hisat2/) (version 2.2.1), aligns sequencing reads to a reference genome.
- **Input:** Illumina sequencing files (`.fastq.gz`), softmasked assembly sequences (`.msk`).
- **Output:** Genome index (`.ht2`), mapping file (`.bam`).
- **Script:** [`./bin/hisat2.sh`](https://gitlab.com/adelfc/nf_assemblify-base/-/blob/master/bin/hisat2.sh)

#### Samtools

- **Tool:** [Samtools](http://www.htslib.org) (version 1.19.2), manipulates high-throughput sequencing data.
- **Input:** Mapping file (`.bam`).
- **Output:** Mapping file index (`.bai`).
- **Script:** [`./bin/samtools.sh`](https://gitlab.com/adelfc/nf_assemblify-base/-/blob/master/bin/samtools.sh)

### Stage 5 - Prediction

- **Tool:** [Braker3](https://github.com/Gaius-Augustus/BRAKER) (version 3.0.3), used for genome structural annotation.
- **Input:** Softmasked assembly sequences (`.msk`), mapping file (`.bam`), proteins from short evolutionary distance (`.faa`)
- **Output:** Structural annotation (`.gff3`), proteins sequences from annotation: (`.faa`)
- **Script:** [`./bin/braker3.sh`](https://gitlab.com/adelfc/nf_assemblify-base/-/blob/master/bin/braker3.sh)

## Running the Pipeline

### Preparing for Execution

Ensure you are on the `master` branch, the main stable branch, for a bug-free pipeline execution. Use the following commands in your terminal:

```
git checkout master
git branch
```

This project was tested on the [IFB cluster](https://login.cluster.france-bioinformatique.fr/dex/auth/ldap/login?back=&state=okszu3py62wfulvummfrqflyv). To allocate the necessary computing resources, IFB users should run:

```
sacctmgr update user $USER set defaultaccount=2401_m2_bim
```

For other clusters or computing environments, please consult your administrator for the equivalent setup.

Navigate to your local copy of the pipeline and prepare for execution:

```
cd /shared/projects/2401_m2_bim/XXX/nf_assemblify-base
vi run_assemblify.slurm
```

In the script `run_assemblify.slurm`, update the working directory path to match your user directory:

```
WK_DIR=/shared/projects/2401_m2_bim/XXX/assemblify-tmp
```
Modify this command by replacing `XXX` with your login number.

### Monitoring the Job

Submit the pipeline for execution using SLURM:

```
sbatch run_assemblify.slurm
```

To monitor the job's progress, use:

```
squeue -u $USER
```

You can also follow the pipeline's log in real time with:

```
tail -f assemblify.log
```

*Please be patient as the log file content might not appear immediately. Exit the log view with CTRL+C.*

### Checking the Execution

Verify the pipeline's successful execution by checking for errors in the log file:

```
tail assembly.log
```

A successful execution will generate a `results` directory, containing output from the different tools. Refer to the "Presentation of the pipeline's project" section for details on these results.

## Licence

This work is licensed under [GNU General Public License v3.0](https://www.gnu.org/licenses/gpl-3.0.en.html#license-text). This license permits others to modify, adjust, and build upon this work for any purpose, provided they credit the author and distribute their derivative creations under the same terms of the GNU General Public License version 3 or later.

## Citation and acknowledgement

### Acknowledgement

This project is part of an introductory course on F.A.I.R. principles at the [University of Rennes](https://www.univ-rennes.fr/). Directed by:

- [Anthony Bretaudeau](https://orcid.org/0000-0003-0914-2470) - INRAE, Rennes
- [Alexandre Cormier](https://orcid.org/0000-0002-7775-8413) - Ifremer, IRSI-SeBiMER, Plouzané, France
- [Erwan Corre](https://orcid.org/0000-0001-6354-2278) - CNRS, Sorbonne Université, ABiMS, Station Biologique, Roscoff, France
- [Patrick Durand](https://orcid.org/0000-0002-5597-4457) - Ifremer, IRSI-SeBiMER, Plouzané, France
- [Gildas  Le Corguillé](https://orcid.org/0000-0003-1742-9711) - Sorbonne Université, CNRS, ABiMS, Station Biologique, Roscoff, France
- [Stéphanie Robin](https://orcid.org/0000-0001-7379-9173) - INRAE, Rennes

Special thanks to the authors of the software packages, pipeline, and public datasets used in this project.

### Citation

For citing this project, refer to the following:

- Project subject on [Ifremer's teaching material](https://practical-case-bioinfo-teaching-fair-gaa-92d332f8346e321adf6fde.gitlab-pages.ifremer.fr/#working-data-sets).
- Associated [training material](https://bioinfo.gitlab-pages.ifremer.fr/teaching/fair-gaa/practical-case/).
- [F.A.I.R principles course](https://fair-gaa-bioinfo-teaching-fair-gaa-4217911ae679179702c067b9f84e.gitlab-pages.ifremer.fr/).

Data sourced from WELLCOME SANGER INSTITUTE, GenBank assembly GCA_914767535.2, processed by ABiMS, Genouest, SeBiMER in January 2024.

This project is registered on [ZENODO](https://zenodo.org/records/10607037), DOI : 10.5281/zenodo.10607037.

## Date

2024-02-02

## Authors

- Maxime Fleury, Master's student in Bioinformatics, University of Rennes - maxime.fleury.1@etudiant.univ-rennes.fr
- Adel Fodil-Cherif, Master's student in Bioinformatics, University of Rennes - adel.fodil-cherif@etudiant.univ-rennes.fr

## Under development: Integration of Braker3

### Overview

We are in the process of incorporating the Braker3 tool into our Nextflow pipeline in order to finalise the pipeline. To facilitate this development and ensure our methods are Findable, Accessible, Interoperable, and Reusable, we have provided the necessary scripts and modules in the `/dev` directory, including `braker.sh` (a Bash script running the tool) and `braker.nf` (a Nextflow module).

### Steps for integration

To integrate Braker3 into the pipeline, modifications are required in several configuration scripts and the main pipeline script. These changes involve specifying paths to essential inputs and adding the Braker3 process for automatic execution by Nextflow:

1. Configuration in `/conf` Directory:
   
   Modify the `custom.config` file to include paths to proteins from closely related species and the name of the species under study. Add the following parameters:
   
   ```
   // Braker proteins path and species name
   braker_prot_path = "${datadir}/*.subset.faa"
   braker_species_name = "Dunaliella primolecta"
   ```

2. Singularity container in `nextflow.config`:
   
   Update the `nextflow.config` file to specify the Singularity container for Braker3. Include this configuration to ensure the process uses the correct environment:
   
   ```
   withName: braker {
       container = "teambraker/braker3:latest"
   }
   ```

3. Process activation in `main.nf`:
   
   In the `main.nf` file, incorporate the Braker3 process to enable its automatic execution within the pipeline. This involves including the Braker3 module and setting up input channels for protein paths and species names:
   
   ```
   include { BRAKER } from './modules/braker.nf'

   /*
    * CREATE INPUT CHANNELS
    */
   braker_prot_ch = Channel.fromPath("${params.braker_prot_path}")
   braker_species_name_ch = Channel.value("${params.braker_species_name}")

   // Execute Braker3
   BRAKER(braker_prot_ch, RED.out.red_ch, HISAT2.out.bam_ch, braker_species_name_ch)
   ```

### Further Information

For comprehensive details about Braker3, including its usage and configuration options, refer to the following resources:

- [Braker3 GitHub Repository](https://github.com/Gaius-Augustus/BRAKER)
- [Braker3 Container](https://hub.docker.com/r/teambraker/braker3/tags)
