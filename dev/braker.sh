!/usr/bin/env bash
###############################################################################
##                                                                           ##
##                    Genome structural annotation                           ##
##                                                                           ##
###############################################################################

# var settings
args=("$@")
NCPUS=${args[0]}
rnaseq=${args[1]}
prot=${args[2]}
genome=${args[3]}
specie=${args[4]}
LOGCMD=${args[5]}


CMD1="braker.pl --species=${specie} --genome=${genome} --bam=${rnaseq} --prot_seq=${prot} --workingdir=braker3 --gff3 --threads ${NCPUS}"

echo $CMD1 > $LOGCMD

eval ${CMD1}
