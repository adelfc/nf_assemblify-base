process braker {

    label 'highmem'

    tag "braker"

    publishDir "${params.resultdir}/06_braker",		mode: 'copy', pattern: '{braker3/*.gff3, braker3/*.faa}'
    publishDir "${params.resultdir}/logs/braker",	mode: 'copy', pattern: 'braker*.log'
    publishDir "${params.outdir}/00_pipeline_info/cmd",	mode: 'copy', pattern: 'braker*.cmd'

    input:
	path(braker_prot_ch)
	path(red_ch)
	path(bam_ch)
	path(braker_species_name_ch)

    output:
	path("braker3/*.gff3")
	path("braker3/*.faa")
	path("braker*.log")
        path("braker*.cmd")

        

    script:
    """
    braker.sh ${task.cpus} ${bam_ch} ${braker_prot_ch} ${red_ch} ${braker_species_name_ch} braker.cmd  >& braker.log 2>&1
    """ 
}

