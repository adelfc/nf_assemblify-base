#!/usr/bin/env bash


# var settings
args=("$@")
BAM=${args[0]}
NCPUS=${args[1]}
LOGCMD=${args[2]}



CMD1="samtools sort -o $BAM -@ $NCPUS -m 2G -O bam -T tmp $BAM"
CMD2="samtools index $BAM"

# Keep command in log
echo $CMD1 > $LOGCMD
echo $CMD2 >> $LOGCMD

# Run Command
eval ${CMD1}
eval ${CMD2}
