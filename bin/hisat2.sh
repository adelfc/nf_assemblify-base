#!/usr/bin/env bash
###############################################################################
##                                                                           ##
##                                                                           ##
##                                                                           ##
###############################################################################

# var settings
args=("$@")
NCPUS=${args[0]}
fasta=${args[1]}
intron=${args[2]}
R1=${args[3]}
R2=${args[4]}
LOGCMD=${args[5]}

bam=$(basename ${R1%.fastq*}).bam

CMD1="hisat2-build -p ${NCPUS} ${fasta} ${fasta%.*}"

CMD2="hisat2 -p ${NCPUS} --no-unal -q -x ${fasta%.*} -1 ${R1} -2 ${R2} --max-intronlen ${intron} > $bam"

echo $CMD1 > $LOGCMD
echo $CMD2 >> $LOGCMD

eval ${CMD1}
eval ${CMD2}
