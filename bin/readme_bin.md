# `/bin` file description

## Overview

In this `/bin` directory, you can find  the different `.sh` files, each serving a critical role for a unique tool in our Nextflow pipeline. When the main.nf script executes, it references these `.sh` files to initiate specific processes. 

## Directory structure 

This section shows the  structure of the `/bin` directory, providing an overview of the Bash scripts used for the different tools of our pipeline.

```
/bin/
├── busco.sh
├── fastqc.sh
├── hifiasm.sh
├── hisat2.sh
├── multiqc.sh
├── nanoplot.sh
├── readme_bin.md
├── red.sh
└── samtools.sh
```

## Pipeline steps

### Stage 1 - Quality control

#### Nanoplot

- **Tool:** [Nanoplot](https://biocontainer-doc.readthedocs.io/en/latest/source/nanoplot/nanoplot.html) (version 1.32.1), a plotting tool for long-read sequencing data.
- **Input:** PacBio sequencing files (`.fastq.gz`).
- **Output:** Web report (`.html`), plots (`.png`), and statistics (`.txt`).
- **Script:** [`./bin/nanoplot.sh`](https://gitlab.com/adelfc/nf_assemblify-base/-/blob/master/bin/nanoplot.sh)

#### Fastqc

- **Tool:** [FastQC](https://www.bioinformatics.babraham.ac.uk/projects/fastqc/) (version 0.11.9), for quality control of high throughput sequence data.
- **Input:** Illumina sequencing files (`.fastq.gz`).
- **Output:** Web report (`.html`), archive (`.zip`).
- **Script:** [`./bin/fastqc.sh`](https://gitlab.com/adelfc/nf_assemblify-base/-/blob/master/bin/fastqc.sh)

#### Multiqc

- **Tool:** [MultiQC](https://multiqc.info) (version 1.14), aggregates results from bioinformatics analyses into a single report.
- **Input:** Archive from FastQC (`.zip`)
- **Output:** Report quality control (`.html`).
- **Script:** [`./bin/multiqc.sh`](https://gitlab.com/adelfc/nf_assemblify-base/-/blob/master/bin/multiqc.sh)

### Stage 2 - Contiging

#### Hifiasm

- **Tool:** [Hifiasm](https://hifiasm.readthedocs.io/en/latest/) (version 0.18.9), for haplotype-resolved de novo assembly.
- **Input:** PacBio sequencing files (`.fastq.gz`).
- **Output:** Assembly sequences (`.fasta`), assembly graph (`.gfa`).
- **Script:** [`./bin/hifiasm.sh`](https://gitlab.com/adelfc/nf_assemblify-base/-/blob/master/bin/hifiasm.sh)

#### Busco

- **Tool:** [BUSCO](https://busco.ezlab.org) (version 5.5.0), assesses genome assembly completeness.
- **Input:** Assembly sequences from Hifiasm (`.fasta`).
- **Output:** Statistics (`.tsv`).
- **Script:** [`./bin/busco.sh`](https://gitlab.com/adelfc/nf_assemblify-base/-/blob/master/bin/busco.sh)

### Stage 3 - Repeat Masking

#### RED

- **Tool:** REDmask (version 2018.09.10), identifies and masks repeats for structural annotation.
- **Input:** Assembly sequences from Hifiasm (`.fasta`).
- **Output:** Softmasked assembly sequences (`.msk`).
- **Script:** [`./bin/red.sh`](https://gitlab.com/adelfc/nf_assemblify-base/-/blob/master/bin/red.sh)

### Stage 4 - Evidences

#### Hisat2

- **Tool:** [Hisat](http://daehwankimlab.github.io/hisat2/) (version 2.2.1), aligns sequencing reads to a reference genome.
- **Input:** Illumina sequencing files (`.fastq.gz`), softmasked assembly sequences (`.msk`).
- **Output:** Genome index (`.ht2`), mapping file (`.bam`).
- **Script:** [`./bin/hisat2.sh`](https://gitlab.com/adelfc/nf_assemblify-base/-/blob/master/bin/hisat2.sh)

#### Samtools

- **Tool:** [Samtools](http://www.htslib.org) (version 1.19.2), manipulates high-throughput sequencing data.
- **Input:** Mapping file (`.bam`).
- **Output:** Mapping file index (`.bai`).
- **Script:** [`./bin/samtools.sh`](https://gitlab.com/adelfc/nf_assemblify-base/-/blob/master/bin/samtools.sh)

### Stage 5 - Prediction

- **Tool:** [Braker3](https://github.com/Gaius-Augustus/BRAKER) (version 3.0.3), used for genome structural annotation.
- **Input:** Softmasked assembly sequences (`.msk`), mapping file (`.bam`), proteins from short evolutionary distance (`.faa`)
- **Output:** Structural annotation (`.gff3`), proteins sequences from annotation: (`.faa`)
- **Script:** [`./bin/braker3.sh`](https://gitlab.com/adelfc/nf_assemblify-base/-/blob/master/bin/braker3.sh)


## Licence

This work is licensed under [GNU General Public License v3.0](https://www.gnu.org/licenses/gpl-3.0.en.html#license-text). This license permits others to modify, adjust, and build upon this work for any purpose, provided they credit the author and distribute their derivative creations under the same terms of the GNU General Public License version 3 or later.

## Citation and acknowledgement

### Acknowledgement

This project is part of an introductory course on F.A.I.R. principles at the [University of Rennes](https://www.univ-rennes.fr/). Directed by:

- [Anthony Bretaudeau](https://orcid.org/0000-0003-0914-2470) - INRAE, Rennes
- [Alexandre Cormier](https://orcid.org/0000-0002-7775-8413) - Ifremer, IRSI-SeBiMER, Plouzané, France
- [Erwan Corre](https://orcid.org/0000-0001-6354-2278) - CNRS, Sorbonne Université, ABiMS, Station Biologique, Roscoff, France
- [Patrick Durand](https://orcid.org/0000-0002-5597-4457) - Ifremer, IRSI-SeBiMER, Plouzané, France
- [Gildas  Le Corguillé](https://orcid.org/0000-0003-1742-9711) - Sorbonne Université, CNRS, ABiMS, Station Biologique, Roscoff, France
- [Stéphanie Robin](https://orcid.org/0000-0001-7379-9173) - INRAE, Rennes

Special thanks to the authors of the software packages, pipeline, and public datasets used in this project.

### Citation

For citing this project, refer to the following:

- Project subject on [Ifremer's teaching material](https://practical-case-bioinfo-teaching-fair-gaa-92d332f8346e321adf6fde.gitlab-pages.ifremer.fr/#working-data-sets).
- Associated [training material](https://bioinfo.gitlab-pages.ifremer.fr/teaching/fair-gaa/practical-case/).
- [F.A.I.R principles course](https://fair-gaa-bioinfo-teaching-fair-gaa-4217911ae679179702c067b9f84e.gitlab-pages.ifremer.fr/).

Data sourced from WELLCOME SANGER INSTITUTE, GenBank assembly GCA_914767535.2, processed by ABiMS, Genouest, SeBiMER in January 2024.

This project is registered on [ZENODO](https://zenodo.org/records/10607037), DOI : 10.5281/zenodo.10607037.

## Date

2024-02-02

## Authors

- Maxime Fleury, Master's student in Bioinformatics, University of Rennes - maxime.fleury.1@etudiant.univ-rennes.fr
- Adel Fodil-Cherif, Master's student in Bioinformatics, University of Rennes - adel.fodil-cherif@etudiant.univ-rennes.fr
